:- module gd.

:- interface.
    :- import_module io.

    % Expose some Godot types
    :- type gd.vector2.
    :- type gd.node2d.
    
    
    :- func create_vector2(float, float) = gd.vector2.
    :- func add(gd.vector2, gd.vector2) = gd.vector2.
    :- func mul(gd.vector2, float) = gd.vector2.
    % :- func mul(float, gd.vector2) = gd.vector2.

    
    :- pred set_position(gd.node2d::in, vector2::in, io::di, io::uo) is det.

:- implementation.
    :- pragma foreign_decl("C#", "using Godot;").

    :- pragma foreign_type("C#", gd.vector2, "Vector2").
    :- pragma foreign_type("C#", gd.node2d, "Node2D").

    :- pragma foreign_proc("C#", create_vector2(X::in, Y::in) = (V::out),
        [promise_pure, will_not_call_mercury],
        "
            V = new Vector2((float) X, (float) Y);
        "
    ).
    
    
    :- pragma foreign_proc("C#", add(U::in, V::in) = (W::out),
        [promise_pure, will_not_call_mercury],
        "
            W = U + V;
        "
    ).

    
    :- pragma foreign_proc("C#", mul(U::in, C::in) = (V::out),
        [promise_pure, will_not_call_mercury],
        "
            var fC = (float) C;
            V = U * fC;
        "    
    ).

   % mul(C, V) = mul(V, C).

   
    :- pragma foreign_proc("C#", set_position(N::in, Pos::in, _IO0::di, _IO::uo),
        [promise_pure, will_not_call_mercury],
        "
            N.SetPosition(Pos);
        "
    ).