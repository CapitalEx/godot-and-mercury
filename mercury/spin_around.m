:- module spin_around.

:- interface.
    :- import_module io.
    :- import_module gd.
    
    :- pred spin(gd.node2d::in, float::in, float::in, io::di, io::uo) is det.

:- implementation.
    :- pragma foreign_decl("C#", "using Godot;").

    :- import_module math.

    spin(Node, Time, Radius, !IO) :-
        X = cos(Time),
        Y = sin(Time),
        Unit = create_vector2(X, Y),
        Pos = mul(Unit, Radius),
        set_position(Node, Pos, !IO).