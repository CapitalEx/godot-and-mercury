using Godot;
using System;
using mercury;

public class SpinAround : Node
{
    [Export]
    NodePath targetPath;
    Node2D target;
    public override void _Ready()
    {
        target = GetNode<Node2D>(targetPath);
    }

    public override void _Process(float delta) 
    {
        spin_around.spin_5_p_0(target, Time.GetTicksMsec() / 500.0, 200.0);    
    }
}
