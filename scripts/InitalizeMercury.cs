using Godot;
using System;
using mercury;

public class InitalizeMercury : Node
{
    public override void _Ready()
    {
        library.ML_std_library_init();
    }
}
