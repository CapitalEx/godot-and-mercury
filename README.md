# Godot and Mercury

![screen shot of the program running in Godot](/screenshots/Screenshot_2022-08-09_23-42-33.png)

A small experiment in using [Mercury](https://mercurylang.org/index.html)'s C# FFI to interact with [Godot Engine](https://godotengine.org/). This project was mostly for fun and to play around with Mercury's style of programming. Additionally, I wanted to test its FFI functionality. The project is split into the following files:

- `InitializeMercury.cs`
- `SpinAround.cs`
- `gd.m`
- `spin_around.m`

# Running the Project
## Depedencies
In order to build this project you must have the following installed:
- C# compiler installed
- A working Mercury compiler with the C# grade installed
- Godot with C# support installed

##  Building
After the dependencies have been installed, run the file `mercury/build.sh`. This will build all the `.m` files into `.cs` files. Additionally, add `mer_std.dll` to the `dlls` folder. The default location of `mer_std.dll` is in `/usr/local/mercury-22.01.3/lib/mercury/lib/csharp` on Linux.

Finally, the project can be built and run through Godot.

# Components
## `InitializeMercury.cs`
```cs
public class InitalizeMercury : Node
{
    public override void _Ready()
    {
        library.ML_std_library_init();
    }
}
```
This file handles preparing the Mercury environment to use with Godot. It simply calls the function `library.ML_std_library_init()` upon Godot's ready step.

## `SpinAround.cs`
```cs
public class SpinAround : Node
{
    [Export]
    NodePath targetPath;
    Node2D target;
    public override void _Ready()
    {
        target = GetNode<Node2D>(targetPath);
    }

    public override void _Process(float delta) 
    {
        spin_around.spin_5_p_0(target, Time.GetTicksMsec() / 500.0, 200.0);    
    }
}

```
This file provides a wrapper around the Mercury library. Mercury programs cannot (technically) reference any outside state. Therefore, a wrapper library is provided to interface with Godot. This wrapper library passes in the target node and time. From there, Mercury handles the rest.

## `gd.m`
```m
:- module gd.

:- interface.
:- import_module io.

:- type gd.vector2.
:- type gd.node2d.

:- func create_vector2(float, float) = gd.vector2.
:- func add(gd.vector2, gd.vector2) = gd.vector2.
:- func mul(gd.vector2, float) = gd.vector2.

:- pred set_position(gd.node2d::in, vector2::in, io::di, io::uo) is det.

:- implementation.
:- pragma foreign_decl("C#", "using Godot;").

:- pragma foreign_type("C#", gd.vector2, "Vector2").
:- pragma foreign_type("C#", gd.node2d, "Node2D").

:- pragma foreign_proc("C#", create_vector2(X::in, Y::in) = (V::out),
    [promise_pure, will_not_call_mercury],
    "
        V = new Vector2((float) X, (float) Y);
    "
).


:- pragma foreign_proc("C#", add(U::in, V::in) = (W::out),
    [promise_pure, will_not_call_mercury],
    "
        W = U + V;
    "
).


:- pragma foreign_proc("C#", mul(U::in, C::in) = (V::out),
    [promise_pure, will_not_call_mercury],
    "
        var fC = (float) C;
        V = U * fC;
    "    
).

% mul(C, V) = mul(V, C).


:- pragma foreign_proc("C#", set_position(N::in, Pos::in, _IO0::di, _IO::uo),
    [promise_pure, will_not_call_mercury],
    "
        N.SetPosition(Pos);
    "
).
```
This file contains bindings to a handful of Godot's API to use within Mercury. Updating node values are treated as an IO operation.

## `spin_arund.m`
```m
:- module spin_around.

:- interface.
:- import_module io.
:- import_module gd.

:- pred spin(gd.node2d::in, float::in, float::in, io::di, io::uo) is det.

:- implementation.
:- pragma foreign_decl("C#", "using Godot;").

:- import_module math.

spin(Node, Time, Radius, !IO) :-
    X = cos(Time),
    Y = sin(Time),
    Unit = create_vector2(X, Y),
    Pos = mul(Unit, Radius),
    set_position(Node, Pos, !IO).
```
This file implements the spinning behavior using the bindings created in `gd.m`. It implements a single function called `spin/5`.

# Challenges
The first and most obvious challenge is the fact Godot's C# interface is designed for OOP. It uses inheritance to derive a behavioral script. Meanwhile, Mercury is designed for pure declarative, functional, logic programming. That means Mercury has no way to directly integrate into the world of Godot. Therefore, a wrapper library has to be used to pass Mercury the state of the outside world.

Additionally, the Mercury compiler has no way to stop you from writing bad FFI. If the FFI is wrong, the resulting CS script will be malformed. Finally, Mercury FFI only supports double floating-point precision. This leads to needing type casts from `double -> float` throughout your FFI code.

C FFI + GDNative could translate into a more direct usage of Mercury and Godot removing the need for wrapper classes. But also, I don't feel like fiddling with GDNative. So, That is left as an exercise for the reader.